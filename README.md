# nisi orcad libraries

This repository contains libraries for orcad collected or created by nisi. 
It includes Capture symbols, PCB editor footprints and padstacks.
Use at your own risk! 

## usage

Clone to your preferred location (keep the path short, cause OrCad doesn't handle long paths very well)

1. Add path to footprints folder to psmpath in pcb editor (setup->user preferences->paths->library)  
2. Add path to padstacks folder to padpath in pcb editor (setup->user preferences->paths->library)  
3. Add path to stepfiles folder to step_facet_path, step_mapping_path, steppath in pcb editor (setup->user preferences->paths->library)  
4. Import capture libraries from placement menu in orcad capture